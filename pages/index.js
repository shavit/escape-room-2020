import { route } from "next/dist/next-server/server/router";
import { useRouter } from "next/router";
import { json as game } from "../utils/game";
import styles from "../styles/app.module.css";

export default () => {
  const router = useRouter();
  const openGame = () => {
    router.push('/game/0');
  }

  return (
    <div className={styles.wrapper}>
      <button className={styles.button} onClick={openGame}>{game.name}</button>
    </div>
  );
}
