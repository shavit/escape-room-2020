import styles from "../../../styles/room.module.css"
import {json as game} from "../../../utils/game";

const FinishPage = () => {
    return (
        <div className={`${styles.introScreen} ${styles.finishPage}`}>
            <div className={styles.confettiContainer}>
                <div className={styles.confetti}></div>
                <div className={styles.confetti}></div>
                <div className={styles.confetti}></div>
                <div className={styles.confetti}></div>
                <div className={styles.confetti}></div>
                <div className={styles.confetti}></div>
                <div className={styles.confetti}></div>
                <div className={styles.confetti}></div>
                <div className={styles.confetti}></div>
                <div className={styles.confetti}></div>
                <div className={styles.confetti}></div>
                <div className={styles.confetti}></div>
                <div className={styles.confetti}></div>
                <div className={styles.confetti}></div>
                <div className={styles.confetti}></div>
                <div className={styles.confetti}></div>
                <div className={styles.confetti}></div>
                <div className={styles.confetti}></div>
                <div className={styles.confetti}></div>
                <div className={styles.confetti}></div>
            </div>
            <h1 className={styles.introText}>{game.successScreen.title}</h1>
            <article className={styles.introText}>
                {game.successScreen.message}
            </article>
            <img className="gameSuccessImage" src={game.successScreen.image} alt=""/>
        </div>
    );
}

export default FinishPage;