import { useState, useEffect } from "react";
import { useRouter } from "next/router";

import { json as game } from "./../../../../../utils/game";

import styles from "../../../../../styles/room.module.css";

export default () => {
  const router = useRouter();
  const [itemId, setItemId] = useState(0);
  const [roomEnd, setRoomEnd] = useState(false);
  const [introViewed, setIntroViewed] = useState(false);
  const [showTransition, setShowTransition] = useState(false);

    const roomId = Number(router.query.roomId) || 0;
    const gameId = Number(router.query.gameId) || 0;
    const roomItemsArr = game.rooms[roomId].items || [];
    const roomImageUrl = game.rooms[roomId].image || "";
    const welcomeText = game.rooms[roomId].welcomeText || "";
    const endingText = game.rooms[roomId].endingText || "";
    const welcomeImage = game.rooms[roomId].welcomeImage || "#";
    const transition = game.rooms[roomId].transition;

  const { type: shape, coords, hint = "" } = roomItemsArr[itemId] || {
    type: "",
    coords: "",
  };

  const handleAreaClick = () => {
    if (itemId < roomItemsArr.length - 1) {
      setItemId(itemId + 1);
    } else {
      setRoomEnd(true);
    }
  };

  const handleIntroClick = () => {
    setIntroViewed(true);
  };

  const handleTransitionEnded = () => {
    setShowTransition(false);
    moveToNextRoom();
  };

  const handleNextClick = () => {
    if (transition) {
      setShowTransition(true);
    } else {
      moveToNextRoom();
    }
  };

  const moveToNextRoom = () => {
    setRoomEnd(false);
    setItemId(0);
    setIntroViewed(false);
    if (game.rooms.length === roomId + 1) {
      router.replace(`/game/${gameId}/finish/`);
    } else {
      router.replace(`/game/${gameId}/room/${roomId + 1}/`);
    }
  };

  return (
    <div className={styles.wrapper}>
      {transition && showTransition && (
        <div className={styles.transitionWrapper}>
          <video autoPlay onEnded={handleTransitionEnded} src={transition} />
        </div>
      )}

      {introViewed ? (
        <div>
          <div className={styles.label}>
            <div className={styles.clueNumber}><span>{itemId + 1}</span></div>
            {hint}
          </div>
          <img
            src={roomImageUrl}
            useMap="#imagemap"
            className={styles.imageMap}
          />
          <map name="imagemap">
            <area
              href="#"
              coords={coords}
              shape={shape}
              onClick={handleAreaClick}
            />
          </map>
          {roomEnd && (
            <div className={styles.roomEnd}>
              {endingText}
              <button onClick={handleNextClick} className={styles.introButton}>
                next &#xbb;
              </button>
            </div>
          )}
        </div>
      ) : (
        <div className={styles.introScreen} onClick={handleIntroClick}>
          <h1 className={styles.introText}>{welcomeText}</h1>
          <img src={welcomeImage} className={styles[`room${roomId}Image`]}/>
          <button className={styles.introButton}>
            next &#xbb;
          </button>
        </div>
      )}
    </div>
  );
};
