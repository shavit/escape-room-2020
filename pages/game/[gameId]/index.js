import { useRouter } from "next/router";
import { json as game } from "../../../utils/game";
import styles from "../../../styles/welcome.module.css";

export default () => {
  const router = useRouter();

  const { gameId } = router.query;

  const onClick = () => {
    router.push(`/game/${gameId}/room/0`);
  };

  return (
    <div className={styles.welcome}>
      <div>
        {game.welcomeScreen.message.split("\n").map((text) => (
          <span>
            {text} <br />
          </span>
        ))}
      </div>
      {game.welcomeScreen.image && (
        <div>
          <img className={styles.image} src={game.welcomeScreen.image} />
        </div>
      )}
      <button onClick={onClick} className={styles.introButton}>
        next &#xbb;
      </button>
    </div>
  );
};
