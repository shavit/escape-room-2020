export const json = {
  name: "Save Lolita",
  welcomeScreen: {
    image:
      "/detective.png",
    message:
      "Hi, your name is Locksher Meshol, a world renowned detective. Lolita the stripper was kidnapped and the captain assigned you to the case. The captain suggested you start by checking out the suspect's house in Madrid.",
  },
  successScreen: {
    image:
      "https://gamersunite.s3.amazonaws.com/uploads/june-s-journey-hidden-object-mystery-game/1513141974431",
    title: "You Found Lolita!",
    message:
      "The man shows you a badge and explains he is an MI6 agent named George Blake. He explains he is also on the case - Lolita is a double agent investigating the mob and he was worried for her life. He suggests you join him, he has a strong lead on her location and was about to drive there right now. You agree (and are thankful there are no more flights) and you drive to a small safe house in the country. You find Lolita there, happy to see you and George.",
  },
  failureScreen: {
    image: "",
    message:
      "You fail to save Lolita, her death is on you! If only you could try again...",
  },
  vaultScreen: {
    image: "",
    message: "",
  },
  totalTimeHours: 1,
  rooms: [
    {
      id: 1,
      image:
        "https://gamersunite.s3.amazonaws.com/uploads/june-s-journey-hidden-object-mystery-game/1508635152407",
      welcomeText: "You arrived at the suspect's house. Looks gloomy...",
      welcomeImage: "/cat.png",
      endingText:
        "You climb using the rope ladder and enter the house through the window.",
      code: "3",
      items: [
        {
          name: "pipe",
          hint:
            "The case file said he is a smoker, maybe you can find some DNA samples here?",
          type: "rect",
          coords: "892,400,945,423",
        },
        {
          name: "keys",
          hint:
            "Great, you got the DNA sample. Looks like someone is inside the house, you should find a way in. You can't open the door, but the window has a lock. If only you had the keys...",
          type: "rect",
          coords: "213,381,185,320",
        },
        {
          name: "pennants",
          hint:
            "OK, got the keys! But the window is too high, you need to find a way to get there, a rope or something similar...",
          type: "poly",
          coords:
            "290,159,326,157,328,265,344,355,348,374,313,376,295,308,287,231,288,177",
        },
      ],
    },
    {
      id: 2,
      image:
        "https://gamersunite.s3.amazonaws.com/uploads/june-s-journey-hidden-object-mystery-game/1508687745445",
      welcomeText:
        "You didn't find anything interesting on the ground floor, so you head down to the basement.",
        welcomeImage: "https://media.istockphoto.com/vectors/stairway-to-success-vector-id535480836?k=6&m=535480836&s=612x612&w=0&h=2Xf0elGD1Nnms4RQTwbz0IPLQ7A9QHobVjMs7y3Yxx4=",
      endingText:
        `You find a flight ticket voucher to Paris in the safe.
        Looks like you have to conquer your fear of flying...`,
      code: "3",
      items: [
        {
          name: "hourglass",
          hint:
            "Time is of the essence, you must hurry - Lolita's life is at stake!",
          type: "rect",
          coords: "737,137,696,61",
        },
        {
          name: "newspaper",
          hint: "Let's see if the case has hit the news.",
          type: "poly",
          coords: "595,309,632,331,611,352,599,341",
        },
        {
          name: "safe",
          hint:
            "Well, the guy isn't here. You could probably find some info about where he went in the safe.",
          type: "poly",
          coords: "190,382,213,418,197,522,158,435,173,388",
        },
      ],
      transition:
        "https://ucarecdn.com/74a0160a-ae12-42cf-9bb8-2fa31fb0e8c3/toparis.mp4",
    },
    {
      id: 3,
      image:
        "https://gamersunite.s3.amazonaws.com/uploads/june-s-journey-hidden-object-mystery-game/1519368200273",
      welcomeText:
        "You landed in Paris (thank god), let's see if we can find some of the suspect's belongings",
        welcomeImage: "https://freepngimg.com/thumb/paris/24121-1-paris-clipart.png",
      endingText:
        "You open the suitcases, and find a Paris hotel voucher in one of them.",
      code: "",
      items: [
        {
          name: "scarf",
          hint: "It's cold in Paris! He probably wears a scarf",
          type: "poly",
          coords: "99,246,109,217,113,273,134,329,92,318,96,267,84,218",
        },
        {
          name: "gloves",
          hint: "A scarf and no gloves is like being naked...",
          type: "rect",
          coords: "827,161,864,209",
        },
        {
          name: "suitcases",
          hint: "Looks like he left in a hurry, his suitcases are still here.",
          type: "rect",
          coords: "323,349,371,386",
        },
      ],
    },
    {
      id: 4,
      image:
        "https://gamersunite.s3.amazonaws.com/uploads/june-s-journey-hidden-object-mystery-game/1520546722321",
      welcomeText:
        "You enter Lolita's hotel room. It is empty, you must have missed her. Let's look for clues about her kidnapper.",
        welcomeImage: "https://media.istockphoto.com/vectors/door-hanger-tags-vector-id509824627?k=6&m=509824627&s=612x612&w=0&h=aFszP8egMQ0p-SR1oYMNe8RnO4c2NaRIyktKCxsoUuU=",
      endingText:
        "You find a tag on the suitcase, directed to Moscow. Geez, another flight...",
      code: "3",
      items: [
        {
          name: "flask",
          hint: "His file states he is an alchoholic",
          type: "poly",
          coords: "373,416,389,415,393,410,398,415,408,416,409,456,372,459",
        },
        {
          name: "bow",
          hint: "You find Lolita's bow here! It must be her room!",
          type: "poly",
          coords:
            "258,96,257,88,262,82,277,92,291,86,297,95,285,99,289,123,279,98,277,128,271,129,270,98",
        },
        {
          name: "luggage tags",
          hint:
            "Maybe you can find a tag that will help you know where she went.",
          type: "rect",
          coords: "681,345,703,381",
        },
      ],
      transition:
        "https://ucarecdn.com/7367e975-5aa3-4e06-8ff8-324782815f75/tomoscow.mp4",
    },
    {
      id: 5,
      image:
        "https://gamersunite.s3.amazonaws.com/uploads/june-s-journey-hidden-object-mystery-game/1520389493590",
      welcomeText: "You arrive at Moscow (and hope there are no more flights!)",
      welcomeImage: "https://www.clipartkey.com/mpngs/m/167-1674745_russia-drawing-moscow-art-russian-buildings-drawing.png",
      endingText:
        "You run down the stairs in hopes to catch him before he fades into the crowds!",
      code: "",
      items: [
        {
          name: "teddybear",
          hint:
            "Lolita's beloved Teddy Bear is here, she must have passed through here!",
          type: "rect",
          coords: "600,297,626,338",
        },
        {
          name: "lady",
          hint:
            "You interogate the suspicious lady staring at you, and ask her if she saw Lolita or the suspect.",
          type: "poly",
          coords:
            "326,243,317,245,317,268,304,312,304,385,319,432,337,430,342,362,361,340,354,297,335,265",
        },
        {
          name: "car",
          hint:
            "Grizelda tells you she didn't see Lolita, but a strange looking man sped down the stairs and got into a yellow car down the street! Hurry, he is getting away!!!",
          type: "rect",
          coords: "745,443,824,500",
        },
      ],
    },
    {
      id: 6,
      image:
        "https://gamersunite.s3.amazonaws.com/uploads/june-s-journey-hidden-object-mystery-game/1508776702953",
      welcomeText:
        "You manage to catch the car in time, luckily the doors are not locked and you manage to squeeze in before the driver can React™.",
        welcomeImage: "https://cdn.icon-icons.com/icons2/2389/PNG/512/react_logo_icon_144942.png",
      endingText:
        "You confront the man with all you have found and demand the truth!",
      code: "3",
      items: [
        {
          name: "photo",
          hint:
            "You spot Lolita's photo, this man must be related to the case.",
          type: "rect",
          coords: "394,185,439,244",
        },
        {
          name: "gun",
          hint: "You also spot a gun, his intentions are clear!",
          type: "poly",
          coords:
            "497,274,494,244,549,221,651,234,645,247,576,239,554,245,533,253,526,245",
        },
        {
          name: "note",
          hint:
            "A note in the car lists Lolita's name, you feel like you are getting close!!!",
          type: "poly",
          coords: "692,279,741,284,733,347,684,342",
        },
      ],
    },
  ],
};
